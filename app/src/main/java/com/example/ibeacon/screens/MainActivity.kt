package com.example.ibeacon.screens

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.RemoteException
import android.util.Log
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ibeacon.Adapters.BeaconAdapter
import com.example.ibeacon.Adapters.BeaconAdapter.ItemBeaconClickListener
import com.example.ibeacon.R
import org.altbeacon.beacon.*
import java.util.logging.Level.INFO


class MainActivity : AppCompatActivity(), BeaconConsumer  {

    private val PERMISSION_REQUEST_FINE_LOCATION = 1
    private val PERMISSION_REQUEST_BACKGROUND_LOCATION = 2
    private val TAG = "MainActivity"
    private val handler = Handler()
    private val SCAN_PERIOD: Long = 10000

    private lateinit var bluetoothAdapter : BluetoothAdapter

    private val bleScanner = object : ScanCallback(){
        override fun onScanResult(callbackType: Int, result: ScanResult?) {

            Log.d(TAG, "onScanResult(): ${result?.device?.address} - ${result?.device?.name}")
            handler.postDelayed({
                var listWithoutDubl = listBLE.distinctBy { it.device.address }
                beaconAdapter.updateBLE(listWithoutDubl.toMutableList())

            }, SCAN_PERIOD)

            listBLE.add(result!!)
        }
    }


    private lateinit var beaconManager: BeaconManager
    private lateinit var listBeacon :  MutableList<org.altbeacon.beacon.Beacon>
    private lateinit var listBLE :  MutableList<ScanResult>
    private lateinit var beaconAdapter : BeaconAdapter
    private lateinit var recyclerViewBeacon : RecyclerView
    private lateinit var switchTypeScan : Switch

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerViewBeacon = findViewById(R.id.recyclerViewBeacons)
        switchTypeScan = findViewById(R.id.switchTypeScan)

        switchTypeScan.setOnCheckedChangeListener { buttonView, isChecked ->
            setTypeScan(isChecked)
            beaconAdapter.changeType(isChecked)
        }

        checkRequest()

        beaconAdapter = BeaconAdapter()
        beaconAdapter.setOnClickRecyclerItem(object : ItemBeaconClickListener{
            override fun onItemClick(position: Int) {
                if (switchTypeScan.isChecked) {
                    var beacon = listBeacon.get(position)
                    var intent = Intent(applicationContext, DetailActivity::class.java)
                    intent.putExtra("UUID", beacon.id1.toString())
                    intent.putExtra("Major", beacon.id2.toString())
                    intent.putExtra("Minor", beacon.id3.toString())
                    intent.putExtra("RSSI", beacon.rssi.toString())
                    intent.putExtra("Distance", beacon.distance.toString())
                    startActivity(intent)  ///!!!
                } else {
                    return
                }
            }
        })

        recyclerViewBeacon.layoutManager = LinearLayoutManager(this)
        recyclerViewBeacon.adapter = beaconAdapter
        listBeacon = mutableListOf()
        listBLE = mutableListOf()
        beaconManager = BeaconManager.getInstanceForApplication(this)
        var bluetoothManager = applicationContext.
            getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter

        switchTypeScan.isChecked = true
        switchTypeScan.isChecked = false
        Log.i(TAG, "scan " )

    }

    private fun checkRequest(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED) {
                if (this.checkSelfPermission(android.Manifest.permission.ACCESS_BACKGROUND_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED
                ) {
                    if (this.shouldShowRequestPermissionRationale(
                                    android.Manifest.permission.ACCESS_BACKGROUND_LOCATION
                            )
                    ) {
                        val builder = AlertDialog.Builder(this)
                        builder.setTitle("В фоне не работает")
                        builder.setMessage("Please grant location access so this app can detect beacons in the background.")
                        builder.setPositiveButton(android.R.string.ok, null)
                        builder.setOnDismissListener {
                            requestPermissions(
                                    arrayOf(
                                            android.Manifest.permission.ACCESS_BACKGROUND_LOCATION
                                    ), PERMISSION_REQUEST_BACKGROUND_LOCATION
                            )
                        }
                        builder.show()
                    } else {
                        val builder = AlertDialog.Builder(this);
                        builder.setTitle("Functionality limited")
                        builder.setMessage("Since background location access has not been granted, this app will not be able to discover beacons in the background.  Please go to Settings -> Applications -> Permissions and grant background location access to this app.");
                        builder.setPositiveButton(android.R.string.ok, null)
                        builder.setOnDismissListener {}
                        builder.show()
                    }
                }
            } else {
                if (this.shouldShowRequestPermissionRationale(android.Manifest.permission.
                        ACCESS_FINE_LOCATION)){
                    requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                            PERMISSION_REQUEST_FINE_LOCATION)
                } else {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Functionality limited")
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons.  Please go to Settings -> Applications -> Permissions and grant location access to this app.")
                    builder.setPositiveButton(android.R.string.ok, null)
                    builder.setOnDismissListener{}
                    builder.show()
                }
            }
        }
    }

    private fun setTypeScan(type: Boolean){
        if (type) {
            scanBeacon()
        } else {
            scanBleDevice()
        }
    }

    private fun scanBleDevice() {
        Toast.makeText(this, "BLE Scaning", Toast.LENGTH_SHORT).show()
        beaconManager.unbind(this)
        bluetoothAdapter.bluetoothLeScanner.startScan(bleScanner)
    }

    private fun scanBeacon(){
        Toast.makeText(this, "Beacon Scaning", Toast.LENGTH_SHORT).show()
        bluetoothAdapter.bluetoothLeScanner.stopScan(bleScanner)
        beaconManager.bind(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (switchTypeScan.isChecked) {
            beaconManager.unbind(this)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_FINE_LOCATION -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Functionality limited")
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons.")
                    builder.setPositiveButton(android.R.string.ok, null)
                    builder.setOnDismissListener {}
                    builder.show()
                }
                return
            }
            PERMISSION_REQUEST_BACKGROUND_LOCATION -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Functionality limited")
                    builder.setMessage("Since background location access has not been granted, this app will not be able to discover beacons when in the background.")
                    builder.setPositiveButton(android.R.string.ok, null)
                    builder.setOnDismissListener {}
                    builder.show()
                }
                return
            }
        }
    }


    override fun onBeaconServiceConnect() {
        val rangeNotifier = RangeNotifier { beacons, region ->
            if (beacons.size > 0) {
                Log.i(TAG, "scan " + beacons.size)

                var sortList = beacons.toMutableList().sortedBy { it.distance }
                listBeacon.clear()
                listBeacon.addAll(sortList)
                beaconAdapter.updateBeacon(listBeacon)
            }
        }
        try {
            beaconManager.startRangingBeaconsInRegion(Region("myRangingUniqueId", null, null, null))
            beaconManager.addRangeNotifier(rangeNotifier)
            beaconManager.startRangingBeaconsInRegion(Region("myRangingUniqueId", null, null, null))
            beaconManager.addRangeNotifier(rangeNotifier)
        } catch (e: RemoteException) {
        }




    }
}

