package com.example.ibeacon.screens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.ibeacon.R

class DetailActivity : AppCompatActivity() {

    private lateinit var textViewUUID : TextView
    private lateinit var textViewMajor : TextView
    private lateinit var textViewMinor : TextView
    private lateinit var textViewRSSI : TextView
    private lateinit var textViewDistance : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        textViewUUID = findViewById(com.example.ibeacon.R.id.textViewUUIDDetail)
        textViewMajor = findViewById(com.example.ibeacon.R.id.textViewMajorDetail)
        textViewMinor = findViewById(com.example.ibeacon.R.id.textViewMinorDetail)
        textViewRSSI = findViewById(com.example.ibeacon.R.id.textViewRSSIDetail)
        textViewDistance = findViewById(com.example.ibeacon.R.id.textViewDistanceDetail)

        var intent = intent
        if (intent != null && intent.hasExtra("UUID")){
            textViewUUID.text = intent.getStringExtra("UUID")
            textViewMajor.text = intent.getStringExtra("Major")
            textViewMinor.text = intent.getStringExtra("Minor")
            textViewRSSI.text = intent.getStringExtra("RSSI")
            textViewDistance.text = intent.getStringExtra("Distance")
        } else {
            finish()
        }
    }
}