package com.example.ibeacon.Adapters

import android.bluetooth.le.ScanResult
import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.RenderProcessGoneDetail
import android.widget.AdapterView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ibeacon.R
import org.w3c.dom.Text
import java.util.*
import java.util.concurrent.BrokenBarrierException

class BeaconAdapter : RecyclerView.Adapter<BeaconAdapter.BeaconViewHolder>() {

    private var listBeacon = mutableListOf<org.altbeacon.beacon.Beacon>()
    private var listBLE = mutableListOf<ScanResult>()
    private var typeScan = false

    lateinit var onItemClickListener : ItemBeaconClickListener

    interface ItemBeaconClickListener{
        fun onItemClick(position: Int)
    }

    fun setOnClickRecyclerItem(onItemClickListener: ItemBeaconClickListener){
        this.onItemClickListener = onItemClickListener
    }

    fun changeType(type: Boolean){
        typeScan = type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeaconViewHolder {
        var view = LayoutInflater.from(parent.context).
            inflate(R.layout.beacon_item, parent,false)
        return BeaconViewHolder(view)
    }

    override fun onBindViewHolder(holder: BeaconViewHolder, position: Int) {
        if (typeScan) {
            var beacon = listBeacon.get(position)
            holder.macLabelTextView.visibility = View.GONE
            holder.titleLabelTextView.visibility = View.VISIBLE
            holder.titleTextView.
            text = beacon.id1.toString() ///UUID
            holder.majorTextView.
            text = beacon.id2.toString() //major
            holder.minorTextView.
            text = beacon.id3.toString() //minor
            holder.rssiTextView.
            text = beacon.rssi.toString() //rssi
            holder.distanceTextView.
            text = beacon.distance.toString() + " m."//distance
        } else {
            var ble = listBLE.get(position)
            holder.macLabelTextView.visibility = View.VISIBLE
            holder.titleLabelTextView.visibility = View.INVISIBLE
            holder.titleTextView.
            text = ble.device.address ///mac
            holder.majorTextView.
            text = ""
            holder.minorTextView.
            text = ""
            holder.rssiTextView.
            text = ble.rssi.toString() //rssi
            holder.distanceTextView.
            text = ""
        }



        holder.itemView.setOnClickListener{
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(position)
            }
        }
    }

    override fun getItemCount(): Int {
        if (typeScan) {
            return listBeacon.size
        } else {
            return listBLE.size
        }
    }

    fun updateBeacon(mListBeacon: MutableList<org.altbeacon.beacon.Beacon>) {
        listBeacon = mListBeacon
        notifyDataSetChanged()
    }

    fun updateBLE(mListBLE: MutableList<ScanResult>) {
        listBLE = mListBLE
        notifyDataSetChanged()
    }

    class BeaconViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var titleLabelTextView : TextView = itemView.findViewById(R.id.textViewTitleLabel)
        var titleTextView : TextView = itemView.findViewById(R.id.textViewTitle)
        var majorTextView : TextView = itemView.findViewById(R.id.textViewMajor)
        var minorTextView : TextView = itemView.findViewById(R.id.textViewMinor)
        var rssiTextView : TextView = itemView.findViewById(R.id.textViewRSSI)
        var distanceTextView : TextView = itemView.findViewById(R.id.textViewDistantion)
        var macLabelTextView : TextView = itemView.findViewById(R.id.textViewMacLabel)



    }
}